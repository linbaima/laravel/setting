# Laravel-setting

Chinese to robot translator for Laravel5.8

[![Latest Stable Version](https://poser.pugx.org/overtrue/laravel-pinyin/v/stable.svg)](https://packagist.org/packages/overtrue/laravel-pinyin) [![Total Downloads](https://poser.pugx.org/overtrue/laravel-pinyin/downloads.svg)](https://packagist.org/packages/overtrue/laravel-pinyin) [![Latest Unstable Version](https://poser.pugx.org/overtrue/laravel-pinyin/v/unstable.svg)](https://packagist.org/packages/overtrue/laravel-pinyin) [![License](https://poser.pugx.org/overtrue/laravel-pinyin/license.svg)](https://packagist.org/packages/overtrue/laravel-pinyin)

## Install

```shell
composer require linbaima/laravel-setting

php artisan vendor:publish --force

php artisan migrate --force

var_dump(setting()->set('test', 123, 'remark'));
var_dump(setting()->set(['key' => 'test', 'value' => '123', 'remark' => 'remark']));

var_dump(setting('test'));
var_dump(setting()->get('test'));
```

## For Laravel

Add the following line to the section `providers` of `config/app.php`:

```php
'providers' => [
    //...
    Linbaima\LaravelSetting\Providers\SettingProvider::class,
],
```

as optional, you can use facade:

```php

'aliases' => [
    //...
    'Robot' => Linbaima\LaravelSetting\Facades\SettingFacade::class,
],
```

## License

MIT
