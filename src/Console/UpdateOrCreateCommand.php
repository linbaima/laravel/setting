<?php

namespace Linbaima\LaravelSetting\Console;

use Illuminate\Console\Command;

/**
 * Class UpdateOrCreateCommand
 *
 * @package Linbaima\LaravelSetting\Console
 */
class UpdateOrCreateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'setting {key}
                            {--value= : 值}
                            {--remark= : 备注}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update or create system config.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $key    = $this->argument('key');
        $value  = $this->option('value');
        $remark = $this->option('remark');

        if ($key) {
            $app = app('Setting');
            if ($app->getQuery()->where('key', $key)->exists()) {
                if ($this->confirm('Confirm update value?')) {
                    $app->set($key, $value, $remark);
                    $this->line('complete update!');
                }
            } else {
                $app->set($key, $value, $remark);
                $this->line('complete create!');
            }
        } else {
            $this->error('key or value not null!');
        }
    }
}
