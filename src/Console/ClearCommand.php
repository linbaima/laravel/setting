<?php

namespace Linbaima\LaravelSetting\Console;

use Illuminate\Console\Command;

/**
 * Class ClearCommand
 *
 * @package Linbaima\LaravelSetting\Console
 */
class ClearCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'setting:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all setting.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        app('Setting')->clear();
        $this->line('complete clear!');
    }
}
