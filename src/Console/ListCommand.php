<?php

namespace Linbaima\LaravelSetting\Console;

use Illuminate\Console\Command;

/**
 * Class ListCommand
 *
 * @package Linbaima\LaravelSetting\Console
 */
class ListCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'setting:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show setting list.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $app  = app('Setting');
        dump($app->all());

//        $this->table([
//            __('名称'),
//            __('备注'),
//            __('键'),
//            __('值'),
//        ], []);
    }
}
