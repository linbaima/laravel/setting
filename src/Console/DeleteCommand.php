<?php

namespace Linbaima\LaravelSetting\Console;

use Illuminate\Console\Command;

/**
 * Class DeleteCommand
 *
 * @package Linbaima\LaravelSetting\Console
 */
class DeleteCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'setting:delete {key}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete setting.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $key = $this->argument('key');
        if ($key) {
            app('Setting')->delete($key);
            $this->line('complete delete!');
        } else {
            $this->error('key not null');
        }
    }
}
