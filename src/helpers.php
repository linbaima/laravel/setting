<?php
if (!function_exists('setting')) {
    /**
     * @param  string|null  $key
     * @param  mixed  $default
     * @return mixed
     */
    function setting($key = null, $default = null)
    {
        $setting = app('Setting');

        if (is_array($key)) {
            $setting->set($key);
        } elseif (!is_null($key)) {
            return $setting->get($key, $default);
        }

        return $setting;
    }
}

if (!function_exists('setting_filter_value')) {
    /**
     * @param  mixed  $value
     * @return mixed
     */
    function setting_filter_value($value)
    {
        if (is_null($value) || $value === '') {
            return null;
        }
        if (is_numeric($value)) {
            return intval($value);
        } elseif (is_string($value) && setting_is_json($value)) {
            return json_decode($value, true);
        } elseif (is_string($value) && in_array($value, ['true', 'false'])) {
            return $value === 'true';
        }
        return $value;
    }
}

if (!function_exists('setting_is_json')) {
    /**
     * @param  string  $json
     * @return bool
     */
    function setting_is_json($json)
    {
        if (empty($json) || !is_string($json)) {
            return false;
        }
        if (PHP_VERSION > 5.3) {
            json_decode($json);
            return (json_last_error() == JSON_ERROR_NONE);
        } else {
            return is_null(json_decode($json)) ? false : true;
        }
    }
}
