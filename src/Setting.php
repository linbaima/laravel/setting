<?php

namespace Linbaima\LaravelSetting;

/**
 * Class Setting
 *
 * @package Linbaima\LaravelSetting
 */
class Setting
{
    public $configs = [];
    public $configCache;
    public $configDatabase;

    /**
     * Setting constructor.
     */
    public function __construct()
    {
        $this->configCache    = config('setting.cache');
        $this->configDatabase = config('setting.database');
    }

    /**
     * @return \Illuminate\Database\Query\Builder|mixed
     */
    public function getQuery()
    {
        return \DB::connection($this->configDatabase['connection'])->table($this->configDatabase['table']);
    }

    /**
     * @param  string  $key
     * @return mixed
     */
    public function getValueByKey($key)
    {
        return $this->getQuery()->where($this->configDatabase['key'], $key)->value($this->configDatabase['value']);
    }

    /**
     * @param  array  $columns
     * @return \Illuminate\Support\Collection
     */
    public function all($columns = ['*'])
    {
        return $this->getQuery()->get($columns);
    }

    /**
     * @param  string  $key
     * @param  mixed  $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        if (!array_key_exists($key, $this->configs)) {
            if ($this->configCache['enable']) {
                $prefix = $this->getCachePrefix($key);
                if (\Cache::has($prefix)) {
                    $this->configs[$key] = \Cache::get($prefix);
                } else {
                    $this->configs[$key] = $this->getValueByKey($key);
                    if ($this->configs[$key]) {
                        \Cache::put($prefix, $this->configs[$key], $this->configCache['minute']);
                    }
                }
            } else {
                $this->configs[$key] = $this->getValueByKey($key);
            }
        }
        return setting_filter_value($this->configs[$key] ?? $default);
    }

    /**
     * @param  string  $key
     * @param  mixed  $value
     * @param  string|null  $remark
     */
    public function set($key, $value = null, $remark = null)
    {
        if (is_array($key)) {
            $value  = $key[$this->configDatabase['value']] ?? null;
            $remark = $key[$this->configDatabase['remark']] ?? null;
            $key    = $key[$this->configDatabase['key']] ?? null;
        }
        if ($key) {
            $this->getQuery()->updateOrInsert([$this->configDatabase['key'] => $key], [
                $this->configDatabase['value']  => $value,
                $this->configDatabase['remark'] => $remark,
            ]);
            $this->clear($key);
        }
    }

    /**
     * @param  string|null  $key
     */
    public function clear($key = null)
    {
        $query = $this->getQuery();
        if (!is_null($key)) {
            $query->where($this->configDatabase['key'], $key);
        }
        $keys = $query->pluck($this->configDatabase['key']);

        foreach ($keys as $_key) {
            if (array_key_exists($_key, $this->configs)) {
                unset($this->configs[$_key]);
            }
            if ($this->configCache['enable']) {
                \Cache::forget($this->getCachePrefix($_key));
            }
        }
    }

    /**
     * @param  string  $key
     * @return mixed
     */
    public function delete($key)
    {
        $this->clear($key);
        return $this->getQuery()->where($this->configDatabase['key'], $key)->delete();
    }

    /**
     * @param  string  $key
     * @return mixed
     */
    public function getCachePrefix($key)
    {
        return $this->configCache['prefix'] . ':' . md5($key);
    }
}
