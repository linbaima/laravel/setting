<?php

namespace Linbaima\LaravelSetting\Facades;

use Illuminate\Support\Facades\Facade as BaseFacade;

/**
 * Class SettingFacade
 *
 * @package Linbaima\LaravelSetting\Facades
 */
class SettingFacade extends BaseFacade
{
    public static function getFacadeAccessor()
    {
        return 'Setting';
    }
}
