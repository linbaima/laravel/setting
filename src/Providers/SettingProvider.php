<?php

namespace Linbaima\LaravelSetting\Providers;

use Illuminate\Support\ServiceProvider;
use Linbaima\LaravelSetting\Console\ClearCommand;
use Linbaima\LaravelSetting\Console\UpdateOrCreateCommand;
use Linbaima\LaravelSetting\Console\DeleteCommand;
use Linbaima\LaravelSetting\Console\ListCommand;
use Linbaima\LaravelSetting\Setting;
use Linbaima\LaravelSetting\SystemConfig;

/**
 * Class SettingProvider
 *
 * @package Linbaima\LaravelSetting\Providers
 */
class SettingProvider extends ServiceProvider
{
    /**
     * Register the provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Setting::class, function ($app) {
            return new Setting();
        });

        $this->app->alias(Setting::class, 'Setting');
    }

    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../config/setting.php'                                              => config_path('setting.php'),
            __DIR__ . '/../../database/migrations/2022_05_31_120103_create_settings_table.php' => database_path('migrations/2022_05_31_120103_create_settings_table.php'),
        ], 'setting');

        if ($this->app->runningInConsole()) {
            $this->commands([
                UpdateOrCreateCommand::class,
                ClearCommand::class,
                ListCommand::class,
                DeleteCommand::class,
            ]);
        }
    }
}
