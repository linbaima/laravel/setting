<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Cache config
    |--------------------------------------------------------------------------
    |
    | Cache related configuration.
    |
    */

    'cache' => [
        'enable' => env('SETTING_CACHE_ENABLE', false),
        'prefix' => env('SETTING_CACHE_PREFIX', 'setting:cache'),
        'minute' => env('SETTING_CACHE_MINUTE', 60),
    ],

    /*
    |--------------------------------------------------------------------------
    | Database config
    |--------------------------------------------------------------------------
    |
    | Database related configuration.
    |
    */

    'database' => [
        'connection' => null,
        'table'      => env('SETTING_TABLE_NAME', 'settings'),
        'key'        => env('SETTING_TABLE_KEY', 'key'),
        'value'      => env('SETTING_TABLE_VALUE', 'value'),
        'remark'     => env('SETTING_TABLE_REMARK', 'remark'),
    ],

];
